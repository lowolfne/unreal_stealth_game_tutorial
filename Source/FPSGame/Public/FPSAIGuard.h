// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSAIGuard.generated.h"

class UPawnSensingComponent;

UENUM(BlueprintType)
enum class EAIState : uint8
{
	Idle,
	Suspicious,
	Alert,
};

UCLASS()
class FPSGAME_API AFPSAIGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSAIGuard();

protected:
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UPawnSensingComponent* PawnSensingComp;

	UPROPERTY(EditInstanceOnly, Category = "AI")
	bool isPatroling;

	UPROPERTY(EditInstanceOnly, Category = "AI", meta = (EditCondition="isPatroling"))
	AActor* FirstPatrolPoint;

	UPROPERTY(EditInstanceOnly, Category = "AI", meta = (EditCondition = "isPatroling"))
	AActor* SecondPatrolPoint;

	AActor* CurrentPatrolPoint;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnSeePawn(APawn* pawn);

	UFUNCTION()
	void OnHearNoise(APawn* pawn, const FVector& location, float volume);

	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
	void OnStateChanged(EAIState state);

	UFUNCTION()
	void OnRep_GuardState();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetState(EAIState state);
private:
	UFUNCTION()
	void ResetOrientation();

	void MoveToNextPatrolPoint();

private:
	FRotator OriginalRotation;

	FTimerHandle ResetOrientationTimerHandle;

	UPROPERTY(ReplicatedUsing=OnRep_GuardState)
	EAIState GuardState;
};
