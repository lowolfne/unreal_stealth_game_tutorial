// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGameState.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "FPSPlayerController.h"

// called everytime
void AFPSGameState::MulticastOnMissionComplete_Implementation(APawn* InstigatorPawn, bool isMissionSuccess)
{
	/*for (FConstPawnIterator it = GetWorld()->GetPawnIterator(); it; it++)
	{
		APawn* pawn = it->Get();

		if (pawn && pawn->IsLocallyControlled())
		{
			pawn->DisableInput(nullptr);
		}
	}*/

	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
	{
		AFPSPlayerController* playerController = Cast<AFPSPlayerController>(it->Get());

		if (playerController == nullptr)
			continue;

		if (playerController->IsLocalController())
		{
			playerController->OnMissionCompleted(InstigatorPawn, isMissionSuccess);
		}

		APawn* pawn = playerController->GetPawn();

		if (pawn)
		{
			pawn->DisableInput(playerController);
		}
	}
}