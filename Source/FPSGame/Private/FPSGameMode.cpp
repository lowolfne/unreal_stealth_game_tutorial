// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSGameMode.h"
#include "FPSHUD.h"
#include "FPSGameState.h"
#include "FPSCharacter.h"
#include "FPSPlayerController.h"

#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

AFPSGameMode::AFPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSHUD::StaticClass();

	GameStateClass = AFPSGameState::StaticClass();
}


void AFPSGameMode::TriggerCompleteMission(APawn* pawn, bool isMissionSuccess)
{
#ifdef VERBOSE
	UE_LOG(LogTemp, Warning, TEXT("Complete Mission"));
#endif
	if (pawn)
	{
		//pawn->DisableInput(nullptr);

		if (SpectatingViewpointClass)
		{
			TArray<AActor*> returnedActors;
			UGameplayStatics::GetAllActorsOfClass(this, SpectatingViewpointClass, returnedActors);

			AActor* viewTarget = nullptr;

			if (returnedActors.Num() > 0)
			{
				viewTarget = returnedActors[0];
			}

			for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
			{
				AFPSPlayerController* playerController = Cast<AFPSPlayerController>(it->Get());
				
				if (playerController && viewTarget )
				{
					playerController->SetViewTargetWithBlend(viewTarget, 0.5f, EViewTargetBlendFunction::VTBlend_Cubic);
				}
			}

			// no need since we want all players to move their camera
			/*APlayerController* playerController = Cast<APlayerController>(pawn->GetController());
			if (playerController && viewTarget)
			{
				playerController->SetViewTargetWithBlend(viewTarget, 0.5f, EViewTargetBlendFunction::VTBlend_Cubic);
			}*/

			if (!isMissionSuccess)
			{
				UE_LOG(LogTemp, Warning, TEXT("Mission Failed"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("SpectatingViewpointClass might be null."));
		}

		AFPSGameState* gameState = GetGameState<AFPSGameState>();

		if (gameState)
		{
			gameState->MulticastOnMissionComplete(pawn, isMissionSuccess);
		}

		OnMissionCompleted(pawn, isMissionSuccess);
	}
}