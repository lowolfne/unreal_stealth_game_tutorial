// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSObjectiveActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Public/FPSCharacter.h"

// Sets default values
AFPSObjectiveActor::AFPSObjectiveActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	// set no collision
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = MeshComp;

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	// reset collision channel
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	// set collision channel
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComp->SetupAttachment(MeshComp);

	SetReplicates(true);
}

// Called when the game starts or when spawned
void AFPSObjectiveActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFPSObjectiveActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFPSObjectiveActor::NotifyActorBeginOverlap(AActor* other)
{
	Super::NotifyActorBeginOverlap(other);

	PlayEffects();

	if (Role == ROLE_Authority) 
	{
		AFPSCharacter* myCharacter = Cast<AFPSCharacter>(other);
		if (myCharacter)
		{
			myCharacter->IsCarryingObjective = true;
			Destroy();
		}
	}
}

void AFPSObjectiveActor::PlayEffects()
{
	if (ClaimFX == NULL)
	{
		return;
	}

	UGameplayStatics::SpawnEmitterAtLocation(this, ClaimFX, GetActorLocation());
}

