// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSAIGuard.h"
#include "FPSGameMode.h"
#include "Perception/PawnSensingComponent.h"
// deprecated 4.20 above 
//#include "NavigationSystem.h"
//#include "NavigationPath.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "DrawDebugHelpers.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AFPSAIGuard::AFPSAIGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
}

// Called when the game starts or when spawned
void AFPSAIGuard::BeginPlay()
{
	Super::BeginPlay();

	if (PawnSensingComp != nullptr)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPSAIGuard::OnSeePawn);
		PawnSensingComp->OnHearNoise.AddDynamic(this, &AFPSAIGuard::OnHearNoise);
	}

	OriginalRotation = GetActorRotation();

	if (isPatroling)
	{
		MoveToNextPatrolPoint();
	}
}

// Called every frame
void AFPSAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentPatrolPoint)
	{
		FVector delta = GetActorLocation() - CurrentPatrolPoint->GetActorLocation();
		float distance = delta.Size2D();
		if (distance < 50.0f)
		{
			MoveToNextPatrolPoint();
		}
	}
}

void AFPSAIGuard::OnSeePawn(APawn* pawn)
{
	if (pawn == nullptr || GuardState == EAIState::Alert )
	{
		return;
	}

	DrawDebugSphere(GetWorld(), pawn->GetActorLocation(), 32.0f, 12, FColor::Yellow, false, 10.0f);

	AFPSGameMode* gameMode = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		gameMode->TriggerCompleteMission(pawn, false);
	}

	SetState(EAIState::Alert);

	AController* controller = GetController();

	if (controller)
	{
		controller->StopMovement();
	}
}

void AFPSAIGuard::OnHearNoise(APawn* pawn, const FVector& location, float volume)
{
	if (pawn == nullptr )
	{
		return;
	}

	DrawDebugSphere(GetWorld(), location, 32.0f, 12, FColor::Cyan, false, 10.0f);

	// location - noise location
	FVector direction = location - GetActorLocation();
	direction.Normalize();

	FRotator newLookAt = FRotationMatrix::MakeFromX(direction).Rotator();
	newLookAt.Pitch = 0.0f;
	newLookAt.Roll = 0.0f;
	SetActorRotation(newLookAt);

	GetWorldTimerManager().ClearTimer(ResetOrientationTimerHandle);
	GetWorldTimerManager().SetTimer(ResetOrientationTimerHandle, this, &AFPSAIGuard::ResetOrientation, 3.0f);

	SetState(EAIState::Suspicious);

	AController* controller = GetController();
	if (controller)
	{
		controller->StopMovement();
	}
}


void AFPSAIGuard::ResetOrientation()
{
	if (GuardState == EAIState::Alert)
	{
		return;
	}

	SetActorRotation(OriginalRotation);

	SetState(EAIState::Idle);

	if (isPatroling)
	{
		MoveToNextPatrolPoint();
	}
}

void AFPSAIGuard::SetState(EAIState state)
{
	if (GuardState == state)
	{
		return;
	}

	GuardState = state;

	OnRep_GuardState();
}

void AFPSAIGuard::MoveToNextPatrolPoint()
{
	if (CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
	{
		CurrentPatrolPoint = FirstPatrolPoint;
	}
	else
	{
		CurrentPatrolPoint = SecondPatrolPoint;
	}

	// deprecated 4.20 above 
	//UNavigationSystemV1::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
	UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
}

void AFPSAIGuard::OnRep_GuardState()
{
	OnStateChanged(GuardState);
}

void AFPSAIGuard::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSAIGuard, GuardState);
}